package businesses

import (
	"context"
	"gitee.com/zxs-micro/zxs-micro-business/grpc/businesses/impl"
	"gitee.com/zxs-micro/zxs-micro-business/protos/businesses"
	"gitee.com/zxs-micro/zxs-micro-common/grpc/common"
	"sync"
)

var bs *BusinessService
var bl sync.Mutex

func NewBusinessService(f1 impl.MicroBusinessFunc) *BusinessService {
	if f1 == nil {
		return nil
	}
	if bs == nil {
		bs = new(BusinessService)
		bs.funcs = f1
	}
	return bs
}

type BusinessService struct {
	common.CommonService
	lock  sync.Mutex
	funcs impl.MicroBusinessFunc
}

func (b *BusinessService) Exec(ctx context.Context, req *businesses.ExecRequest) (*businesses.ExecResult, error) {
	var err error
	payload, err := b.funcs.Exec(req.Token, req.Params)
	return &businesses.ExecResult{
		Payload: payload,
	}, err
}

func (b *BusinessService) SelfExec(token string, params [][]byte) ([]byte, error) {
	return b.funcs.Exec(token, params)
}

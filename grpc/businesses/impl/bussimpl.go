package impl

//输入参数分别为用户token和请求参数param
type BusinessFunc = func(string, []byte) ([]byte, error)

//输入参数分别为用户token和请求参数param
type MicroBusinessFunc interface {
	Init() error
	Exec(string, [][]byte) ([]byte, error)
}

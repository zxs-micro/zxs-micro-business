package service

import (
	"gitee.com/zxs-micro/zxs-micro-common/grpc"
	"github.com/spf13/viper"
)

func GetBusinessConfig() grpc.GrpcServerConfig {
	conf := grpc.GrpcServerConfig{}
	conf.ConnTimeout = viper.GetDuration("server.conntimeout")
	conf.GrpcSecurity = grpc.GrpcSecurity{
		UseTls: viper.GetBool("server.tls.enable"),
	}
	if conf.UseTls {
		//todo 使用tls，读取配置中的证书路径
	}
	conf.GrpcKeepAlive = grpc.DefaultKeepaliveOptions
	if viper.IsSet("server.keepalive.interval") {
		conf.ServerInterval = viper.GetDuration("server.keepalive.interval")
	}
	if viper.IsSet("server.keepalive.timeout") {
		conf.ServerInterval = viper.GetDuration("server.keepalive.timeout")
	}
	if viper.IsSet("server.keepalive.mininterval") {
		conf.ServerInterval = viper.GetDuration("server.keepalive.mininterval")
	}
	return conf
}

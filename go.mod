module gitee.com/zxs-micro/zxs-micro-business

go 1.16

require (
	gitee.com/zxs-micro/zxs-micro-common v0.0.1
	gitee.com/zxs-micro/zxs-micro-fuwufaxian v0.0.1
	github.com/gin-gonic/gin v1.7.4
	github.com/spf13/viper v1.9.0
	go.uber.org/zap v1.19.1
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
